/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_fact_image_urlUpdateComponent } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url-update.component';
import { Alexa_fact_image_urlService } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url.service';
import { Alexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';

describe('Component Tests', () => {
    describe('Alexa_fact_image_url Management Update Component', () => {
        let comp: Alexa_fact_image_urlUpdateComponent;
        let fixture: ComponentFixture<Alexa_fact_image_urlUpdateComponent>;
        let service: Alexa_fact_image_urlService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_fact_image_urlUpdateComponent]
            })
                .overrideTemplate(Alexa_fact_image_urlUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Alexa_fact_image_urlUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Alexa_fact_image_urlService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Alexa_fact_image_url(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.alexa_fact_image_url = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Alexa_fact_image_url();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.alexa_fact_image_url = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
