/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_fact_image_urlDeleteDialogComponent } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url-delete-dialog.component';
import { Alexa_fact_image_urlService } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url.service';

describe('Component Tests', () => {
    describe('Alexa_fact_image_url Management Delete Component', () => {
        let comp: Alexa_fact_image_urlDeleteDialogComponent;
        let fixture: ComponentFixture<Alexa_fact_image_urlDeleteDialogComponent>;
        let service: Alexa_fact_image_urlService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_fact_image_urlDeleteDialogComponent]
            })
                .overrideTemplate(Alexa_fact_image_urlDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Alexa_fact_image_urlDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Alexa_fact_image_urlService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
