/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_fact_image_urlComponent } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url.component';
import { Alexa_fact_image_urlService } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url.service';
import { Alexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';

describe('Component Tests', () => {
    describe('Alexa_fact_image_url Management Component', () => {
        let comp: Alexa_fact_image_urlComponent;
        let fixture: ComponentFixture<Alexa_fact_image_urlComponent>;
        let service: Alexa_fact_image_urlService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_fact_image_urlComponent],
                providers: []
            })
                .overrideTemplate(Alexa_fact_image_urlComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Alexa_fact_image_urlComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Alexa_fact_image_urlService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Alexa_fact_image_url(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.alexa_fact_image_urls[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
