/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_fact_image_urlDetailComponent } from 'app/entities/service4/alexa-fact-image-url/alexa-fact-image-url-detail.component';
import { Alexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';

describe('Component Tests', () => {
    describe('Alexa_fact_image_url Management Detail Component', () => {
        let comp: Alexa_fact_image_urlDetailComponent;
        let fixture: ComponentFixture<Alexa_fact_image_urlDetailComponent>;
        const route = ({ data: of({ alexa_fact_image_url: new Alexa_fact_image_url(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_fact_image_urlDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(Alexa_fact_image_urlDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Alexa_fact_image_urlDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.alexa_fact_image_url).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
