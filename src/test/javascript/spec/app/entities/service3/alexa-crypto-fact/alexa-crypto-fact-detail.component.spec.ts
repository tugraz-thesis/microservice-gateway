/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_crypto_factDetailComponent } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact-detail.component';
import { Alexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';

describe('Component Tests', () => {
    describe('Alexa_crypto_fact Management Detail Component', () => {
        let comp: Alexa_crypto_factDetailComponent;
        let fixture: ComponentFixture<Alexa_crypto_factDetailComponent>;
        const route = ({ data: of({ alexa_crypto_fact: new Alexa_crypto_fact(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_crypto_factDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(Alexa_crypto_factDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Alexa_crypto_factDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.alexa_crypto_fact).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
