/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_crypto_factDeleteDialogComponent } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact-delete-dialog.component';
import { Alexa_crypto_factService } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact.service';

describe('Component Tests', () => {
    describe('Alexa_crypto_fact Management Delete Component', () => {
        let comp: Alexa_crypto_factDeleteDialogComponent;
        let fixture: ComponentFixture<Alexa_crypto_factDeleteDialogComponent>;
        let service: Alexa_crypto_factService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_crypto_factDeleteDialogComponent]
            })
                .overrideTemplate(Alexa_crypto_factDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Alexa_crypto_factDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Alexa_crypto_factService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
