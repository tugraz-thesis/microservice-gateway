/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_crypto_factUpdateComponent } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact-update.component';
import { Alexa_crypto_factService } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact.service';
import { Alexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';

describe('Component Tests', () => {
    describe('Alexa_crypto_fact Management Update Component', () => {
        let comp: Alexa_crypto_factUpdateComponent;
        let fixture: ComponentFixture<Alexa_crypto_factUpdateComponent>;
        let service: Alexa_crypto_factService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_crypto_factUpdateComponent]
            })
                .overrideTemplate(Alexa_crypto_factUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Alexa_crypto_factUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Alexa_crypto_factService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Alexa_crypto_fact(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.alexa_crypto_fact = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Alexa_crypto_fact();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.alexa_crypto_fact = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
