/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { Alexa_crypto_factComponent } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact.component';
import { Alexa_crypto_factService } from 'app/entities/service3/alexa-crypto-fact/alexa-crypto-fact.service';
import { Alexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';

describe('Component Tests', () => {
    describe('Alexa_crypto_fact Management Component', () => {
        let comp: Alexa_crypto_factComponent;
        let fixture: ComponentFixture<Alexa_crypto_factComponent>;
        let service: Alexa_crypto_factService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [Alexa_crypto_factComponent],
                providers: []
            })
                .overrideTemplate(Alexa_crypto_factComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Alexa_crypto_factComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Alexa_crypto_factService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Alexa_crypto_fact(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.alexa_crypto_facts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
