export interface ISkill {
    id?: number;
    factdescription?: string;
}

export class Skill implements ISkill {
    constructor(public id?: number, public factdescription?: string) {}
}
