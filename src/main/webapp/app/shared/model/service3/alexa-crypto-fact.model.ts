export interface IAlexa_crypto_fact {
    id?: number;
    factText?: string;
    id_image_url?: number;
}

export class Alexa_crypto_fact implements IAlexa_crypto_fact {
    constructor(public id?: number, public factText?: string, public id_image_url?: number) {}
}
