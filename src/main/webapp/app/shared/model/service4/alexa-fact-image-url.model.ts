export interface IAlexa_fact_image_url {
    id?: number;
    url?: string;
}

export class Alexa_fact_image_url implements IAlexa_fact_image_url {
    constructor(public id?: number, public url?: string) {}
}
