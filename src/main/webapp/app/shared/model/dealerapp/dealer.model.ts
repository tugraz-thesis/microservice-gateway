export interface IDealer {
    id?: number;
    dealerbrand?: string;
}

export class Dealer implements IDealer {
    constructor(public id?: number, public dealerbrand?: string) {}
}
