import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAlexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';
import { Principal } from 'app/core';
import { Alexa_crypto_factService } from './alexa-crypto-fact.service';

@Component({
    selector: 'jhi-alexa-crypto-fact',
    templateUrl: './alexa-crypto-fact.component.html'
})
export class Alexa_crypto_factComponent implements OnInit, OnDestroy {
    alexa_crypto_facts: IAlexa_crypto_fact[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private alexa_crypto_factService: Alexa_crypto_factService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.alexa_crypto_factService.query().subscribe(
            (res: HttpResponse<IAlexa_crypto_fact[]>) => {
                this.alexa_crypto_facts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAlexa_crypto_facts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAlexa_crypto_fact) {
        return item.id;
    }

    registerChangeInAlexa_crypto_facts() {
        this.eventSubscriber = this.eventManager.subscribe('alexa_crypto_factListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
