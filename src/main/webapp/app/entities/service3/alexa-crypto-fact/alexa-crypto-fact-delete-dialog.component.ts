import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAlexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';
import { Alexa_crypto_factService } from './alexa-crypto-fact.service';

@Component({
    selector: 'jhi-alexa-crypto-fact-delete-dialog',
    templateUrl: './alexa-crypto-fact-delete-dialog.component.html'
})
export class Alexa_crypto_factDeleteDialogComponent {
    alexa_crypto_fact: IAlexa_crypto_fact;

    constructor(
        private alexa_crypto_factService: Alexa_crypto_factService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.alexa_crypto_factService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'alexa_crypto_factListModification',
                content: 'Deleted an alexa_crypto_fact'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-alexa-crypto-fact-delete-popup',
    template: ''
})
export class Alexa_crypto_factDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ alexa_crypto_fact }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(Alexa_crypto_factDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.alexa_crypto_fact = alexa_crypto_fact;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
