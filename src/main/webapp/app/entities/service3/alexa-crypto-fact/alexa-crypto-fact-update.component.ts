import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IAlexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';
import { Alexa_crypto_factService } from './alexa-crypto-fact.service';

@Component({
    selector: 'jhi-alexa-crypto-fact-update',
    templateUrl: './alexa-crypto-fact-update.component.html'
})
export class Alexa_crypto_factUpdateComponent implements OnInit {
    private _alexa_crypto_fact: IAlexa_crypto_fact;
    isSaving: boolean;

    constructor(private alexa_crypto_factService: Alexa_crypto_factService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ alexa_crypto_fact }) => {
            this.alexa_crypto_fact = alexa_crypto_fact;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.alexa_crypto_fact.id !== undefined) {
            this.subscribeToSaveResponse(this.alexa_crypto_factService.update(this.alexa_crypto_fact));
        } else {
            this.subscribeToSaveResponse(this.alexa_crypto_factService.create(this.alexa_crypto_fact));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAlexa_crypto_fact>>) {
        result.subscribe((res: HttpResponse<IAlexa_crypto_fact>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get alexa_crypto_fact() {
        return this._alexa_crypto_fact;
    }

    set alexa_crypto_fact(alexa_crypto_fact: IAlexa_crypto_fact) {
        this._alexa_crypto_fact = alexa_crypto_fact;
    }
}
