import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAlexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';

@Component({
    selector: 'jhi-alexa-crypto-fact-detail',
    templateUrl: './alexa-crypto-fact-detail.component.html'
})
export class Alexa_crypto_factDetailComponent implements OnInit {
    alexa_crypto_fact: IAlexa_crypto_fact;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ alexa_crypto_fact }) => {
            this.alexa_crypto_fact = alexa_crypto_fact;
        });
    }

    previousState() {
        window.history.back();
    }
}
