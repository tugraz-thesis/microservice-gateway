import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAlexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';

type EntityResponseType = HttpResponse<IAlexa_crypto_fact>;
type EntityArrayResponseType = HttpResponse<IAlexa_crypto_fact[]>;

@Injectable({ providedIn: 'root' })
export class Alexa_crypto_factService {
    private resourceUrl = SERVER_API_URL + 'service3/api/alexa-crypto-facts';

    constructor(private http: HttpClient) {}

    create(alexa_crypto_fact: IAlexa_crypto_fact): Observable<EntityResponseType> {
        return this.http.post<IAlexa_crypto_fact>(this.resourceUrl, alexa_crypto_fact, { observe: 'response' });
    }

    update(alexa_crypto_fact: IAlexa_crypto_fact): Observable<EntityResponseType> {
        return this.http.put<IAlexa_crypto_fact>(this.resourceUrl, alexa_crypto_fact, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAlexa_crypto_fact>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAlexa_crypto_fact[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
