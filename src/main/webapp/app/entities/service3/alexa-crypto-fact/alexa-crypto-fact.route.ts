import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Alexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';
import { Alexa_crypto_factService } from './alexa-crypto-fact.service';
import { Alexa_crypto_factComponent } from './alexa-crypto-fact.component';
import { Alexa_crypto_factDetailComponent } from './alexa-crypto-fact-detail.component';
import { Alexa_crypto_factUpdateComponent } from './alexa-crypto-fact-update.component';
import { Alexa_crypto_factDeletePopupComponent } from './alexa-crypto-fact-delete-dialog.component';
import { IAlexa_crypto_fact } from 'app/shared/model/service3/alexa-crypto-fact.model';

@Injectable({ providedIn: 'root' })
export class Alexa_crypto_factResolve implements Resolve<IAlexa_crypto_fact> {
    constructor(private service: Alexa_crypto_factService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((alexa_crypto_fact: HttpResponse<Alexa_crypto_fact>) => alexa_crypto_fact.body));
        }
        return of(new Alexa_crypto_fact());
    }
}

export const alexa_crypto_factRoute: Routes = [
    {
        path: 'alexa-crypto-fact',
        component: Alexa_crypto_factComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service3AlexaCryptoFact.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'alexa-crypto-fact/:id/view',
        component: Alexa_crypto_factDetailComponent,
        resolve: {
            alexa_crypto_fact: Alexa_crypto_factResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service3AlexaCryptoFact.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'alexa-crypto-fact/new',
        component: Alexa_crypto_factUpdateComponent,
        resolve: {
            alexa_crypto_fact: Alexa_crypto_factResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service3AlexaCryptoFact.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'alexa-crypto-fact/:id/edit',
        component: Alexa_crypto_factUpdateComponent,
        resolve: {
            alexa_crypto_fact: Alexa_crypto_factResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service3AlexaCryptoFact.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const alexa_crypto_factPopupRoute: Routes = [
    {
        path: 'alexa-crypto-fact/:id/delete',
        component: Alexa_crypto_factDeletePopupComponent,
        resolve: {
            alexa_crypto_fact: Alexa_crypto_factResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service3AlexaCryptoFact.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
