import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    Alexa_crypto_factComponent,
    Alexa_crypto_factDetailComponent,
    Alexa_crypto_factUpdateComponent,
    Alexa_crypto_factDeletePopupComponent,
    Alexa_crypto_factDeleteDialogComponent,
    alexa_crypto_factRoute,
    alexa_crypto_factPopupRoute
} from './';

const ENTITY_STATES = [...alexa_crypto_factRoute, ...alexa_crypto_factPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        Alexa_crypto_factComponent,
        Alexa_crypto_factDetailComponent,
        Alexa_crypto_factUpdateComponent,
        Alexa_crypto_factDeleteDialogComponent,
        Alexa_crypto_factDeletePopupComponent
    ],
    entryComponents: [
        Alexa_crypto_factComponent,
        Alexa_crypto_factUpdateComponent,
        Alexa_crypto_factDeleteDialogComponent,
        Alexa_crypto_factDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAlexa_crypto_factModule {}
