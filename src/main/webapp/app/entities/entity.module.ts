import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GatewayCarModule as CarappCarModule } from './carapp/car/car.module';
import { GatewayDealerModule as DealerappDealerModule } from './dealerapp/dealer/dealer.module';
import { GatewaySkillModule as AlexaappSkillModule } from './alexaapp/skill/skill.module';
import { GatewayAlexa_crypto_factModule as Service3Alexa_crypto_factModule } from './service3/alexa-crypto-fact/alexa-crypto-fact.module';
import { GatewayAlexa_fact_image_urlModule as Service4Alexa_fact_image_urlModule } from './service4/alexa-fact-image-url/alexa-fact-image-url.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        CarappCarModule,
        DealerappDealerModule,
        AlexaappSkillModule,
        Service3Alexa_crypto_factModule,
        Service4Alexa_fact_image_urlModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
