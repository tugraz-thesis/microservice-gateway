import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAlexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';

type EntityResponseType = HttpResponse<IAlexa_fact_image_url>;
type EntityArrayResponseType = HttpResponse<IAlexa_fact_image_url[]>;

@Injectable({ providedIn: 'root' })
export class Alexa_fact_image_urlService {
    private resourceUrl = SERVER_API_URL + 'service4/api/alexa-fact-image-urls';

    constructor(private http: HttpClient) {}

    create(alexa_fact_image_url: IAlexa_fact_image_url): Observable<EntityResponseType> {
        return this.http.post<IAlexa_fact_image_url>(this.resourceUrl, alexa_fact_image_url, { observe: 'response' });
    }

    update(alexa_fact_image_url: IAlexa_fact_image_url): Observable<EntityResponseType> {
        return this.http.put<IAlexa_fact_image_url>(this.resourceUrl, alexa_fact_image_url, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAlexa_fact_image_url>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAlexa_fact_image_url[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
