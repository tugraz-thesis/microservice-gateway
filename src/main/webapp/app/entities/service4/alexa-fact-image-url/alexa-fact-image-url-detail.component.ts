import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAlexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';

@Component({
    selector: 'jhi-alexa-fact-image-url-detail',
    templateUrl: './alexa-fact-image-url-detail.component.html'
})
export class Alexa_fact_image_urlDetailComponent implements OnInit {
    alexa_fact_image_url: IAlexa_fact_image_url;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ alexa_fact_image_url }) => {
            this.alexa_fact_image_url = alexa_fact_image_url;
        });
    }

    previousState() {
        window.history.back();
    }
}
