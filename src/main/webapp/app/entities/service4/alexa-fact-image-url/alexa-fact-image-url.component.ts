import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAlexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';
import { Principal } from 'app/core';
import { Alexa_fact_image_urlService } from './alexa-fact-image-url.service';

@Component({
    selector: 'jhi-alexa-fact-image-url',
    templateUrl: './alexa-fact-image-url.component.html'
})
export class Alexa_fact_image_urlComponent implements OnInit, OnDestroy {
    alexa_fact_image_urls: IAlexa_fact_image_url[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private alexa_fact_image_urlService: Alexa_fact_image_urlService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.alexa_fact_image_urlService.query().subscribe(
            (res: HttpResponse<IAlexa_fact_image_url[]>) => {
                this.alexa_fact_image_urls = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAlexa_fact_image_urls();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAlexa_fact_image_url) {
        return item.id;
    }

    registerChangeInAlexa_fact_image_urls() {
        this.eventSubscriber = this.eventManager.subscribe('alexa_fact_image_urlListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
