import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    Alexa_fact_image_urlComponent,
    Alexa_fact_image_urlDetailComponent,
    Alexa_fact_image_urlUpdateComponent,
    Alexa_fact_image_urlDeletePopupComponent,
    Alexa_fact_image_urlDeleteDialogComponent,
    alexa_fact_image_urlRoute,
    alexa_fact_image_urlPopupRoute
} from './';

const ENTITY_STATES = [...alexa_fact_image_urlRoute, ...alexa_fact_image_urlPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        Alexa_fact_image_urlComponent,
        Alexa_fact_image_urlDetailComponent,
        Alexa_fact_image_urlUpdateComponent,
        Alexa_fact_image_urlDeleteDialogComponent,
        Alexa_fact_image_urlDeletePopupComponent
    ],
    entryComponents: [
        Alexa_fact_image_urlComponent,
        Alexa_fact_image_urlUpdateComponent,
        Alexa_fact_image_urlDeleteDialogComponent,
        Alexa_fact_image_urlDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAlexa_fact_image_urlModule {}
