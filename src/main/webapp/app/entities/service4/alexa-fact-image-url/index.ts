export * from './alexa-fact-image-url.service';
export * from './alexa-fact-image-url-update.component';
export * from './alexa-fact-image-url-delete-dialog.component';
export * from './alexa-fact-image-url-detail.component';
export * from './alexa-fact-image-url.component';
export * from './alexa-fact-image-url.route';
