import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAlexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';
import { Alexa_fact_image_urlService } from './alexa-fact-image-url.service';

@Component({
    selector: 'jhi-alexa-fact-image-url-delete-dialog',
    templateUrl: './alexa-fact-image-url-delete-dialog.component.html'
})
export class Alexa_fact_image_urlDeleteDialogComponent {
    alexa_fact_image_url: IAlexa_fact_image_url;

    constructor(
        private alexa_fact_image_urlService: Alexa_fact_image_urlService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.alexa_fact_image_urlService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'alexa_fact_image_urlListModification',
                content: 'Deleted an alexa_fact_image_url'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-alexa-fact-image-url-delete-popup',
    template: ''
})
export class Alexa_fact_image_urlDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ alexa_fact_image_url }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(Alexa_fact_image_urlDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.alexa_fact_image_url = alexa_fact_image_url;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
