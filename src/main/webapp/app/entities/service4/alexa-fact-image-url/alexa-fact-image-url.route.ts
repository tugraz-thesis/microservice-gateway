import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Alexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';
import { Alexa_fact_image_urlService } from './alexa-fact-image-url.service';
import { Alexa_fact_image_urlComponent } from './alexa-fact-image-url.component';
import { Alexa_fact_image_urlDetailComponent } from './alexa-fact-image-url-detail.component';
import { Alexa_fact_image_urlUpdateComponent } from './alexa-fact-image-url-update.component';
import { Alexa_fact_image_urlDeletePopupComponent } from './alexa-fact-image-url-delete-dialog.component';
import { IAlexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';

@Injectable({ providedIn: 'root' })
export class Alexa_fact_image_urlResolve implements Resolve<IAlexa_fact_image_url> {
    constructor(private service: Alexa_fact_image_urlService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((alexa_fact_image_url: HttpResponse<Alexa_fact_image_url>) => alexa_fact_image_url.body));
        }
        return of(new Alexa_fact_image_url());
    }
}

export const alexa_fact_image_urlRoute: Routes = [
    {
        path: 'alexa-fact-image-url',
        component: Alexa_fact_image_urlComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service4AlexaFactImageUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'alexa-fact-image-url/:id/view',
        component: Alexa_fact_image_urlDetailComponent,
        resolve: {
            alexa_fact_image_url: Alexa_fact_image_urlResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service4AlexaFactImageUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'alexa-fact-image-url/new',
        component: Alexa_fact_image_urlUpdateComponent,
        resolve: {
            alexa_fact_image_url: Alexa_fact_image_urlResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service4AlexaFactImageUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'alexa-fact-image-url/:id/edit',
        component: Alexa_fact_image_urlUpdateComponent,
        resolve: {
            alexa_fact_image_url: Alexa_fact_image_urlResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service4AlexaFactImageUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const alexa_fact_image_urlPopupRoute: Routes = [
    {
        path: 'alexa-fact-image-url/:id/delete',
        component: Alexa_fact_image_urlDeletePopupComponent,
        resolve: {
            alexa_fact_image_url: Alexa_fact_image_urlResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.service4AlexaFactImageUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
