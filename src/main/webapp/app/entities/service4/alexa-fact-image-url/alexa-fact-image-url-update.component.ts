import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IAlexa_fact_image_url } from 'app/shared/model/service4/alexa-fact-image-url.model';
import { Alexa_fact_image_urlService } from './alexa-fact-image-url.service';

@Component({
    selector: 'jhi-alexa-fact-image-url-update',
    templateUrl: './alexa-fact-image-url-update.component.html'
})
export class Alexa_fact_image_urlUpdateComponent implements OnInit {
    private _alexa_fact_image_url: IAlexa_fact_image_url;
    isSaving: boolean;

    constructor(private alexa_fact_image_urlService: Alexa_fact_image_urlService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ alexa_fact_image_url }) => {
            this.alexa_fact_image_url = alexa_fact_image_url;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.alexa_fact_image_url.id !== undefined) {
            this.subscribeToSaveResponse(this.alexa_fact_image_urlService.update(this.alexa_fact_image_url));
        } else {
            this.subscribeToSaveResponse(this.alexa_fact_image_urlService.create(this.alexa_fact_image_url));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAlexa_fact_image_url>>) {
        result.subscribe(
            (res: HttpResponse<IAlexa_fact_image_url>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get alexa_fact_image_url() {
        return this._alexa_fact_image_url;
    }

    set alexa_fact_image_url(alexa_fact_image_url: IAlexa_fact_image_url) {
        this._alexa_fact_image_url = alexa_fact_image_url;
    }
}
