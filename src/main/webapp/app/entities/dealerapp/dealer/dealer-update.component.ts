import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDealer } from 'app/shared/model/dealerapp/dealer.model';
import { DealerService } from './dealer.service';

@Component({
    selector: 'jhi-dealer-update',
    templateUrl: './dealer-update.component.html'
})
export class DealerUpdateComponent implements OnInit {
    private _dealer: IDealer;
    isSaving: boolean;

    constructor(private dealerService: DealerService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ dealer }) => {
            this.dealer = dealer;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.dealer.id !== undefined) {
            this.subscribeToSaveResponse(this.dealerService.update(this.dealer));
        } else {
            this.subscribeToSaveResponse(this.dealerService.create(this.dealer));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDealer>>) {
        result.subscribe((res: HttpResponse<IDealer>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get dealer() {
        return this._dealer;
    }

    set dealer(dealer: IDealer) {
        this._dealer = dealer;
    }
}
