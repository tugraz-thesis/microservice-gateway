import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Dealer } from 'app/shared/model/dealerapp/dealer.model';
import { DealerService } from './dealer.service';
import { DealerComponent } from './dealer.component';
import { DealerDetailComponent } from './dealer-detail.component';
import { DealerUpdateComponent } from './dealer-update.component';
import { DealerDeletePopupComponent } from './dealer-delete-dialog.component';
import { IDealer } from 'app/shared/model/dealerapp/dealer.model';

@Injectable({ providedIn: 'root' })
export class DealerResolve implements Resolve<IDealer> {
    constructor(private service: DealerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((dealer: HttpResponse<Dealer>) => dealer.body));
        }
        return of(new Dealer());
    }
}

export const dealerRoute: Routes = [
    {
        path: 'dealer',
        component: DealerComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gatewayApp.dealerappDealer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dealer/:id/view',
        component: DealerDetailComponent,
        resolve: {
            dealer: DealerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.dealerappDealer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dealer/new',
        component: DealerUpdateComponent,
        resolve: {
            dealer: DealerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.dealerappDealer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dealer/:id/edit',
        component: DealerUpdateComponent,
        resolve: {
            dealer: DealerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.dealerappDealer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dealerPopupRoute: Routes = [
    {
        path: 'dealer/:id/delete',
        component: DealerDeletePopupComponent,
        resolve: {
            dealer: DealerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.dealerappDealer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
